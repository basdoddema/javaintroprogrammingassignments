package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    double euclideanDistanceTo(Point otherPoint) {
        //calculate distance - can you implement this?
        return Math.sqrt((otherPoint.y - y) * (otherPoint.y - y) + (otherPoint.x - x) * (otherPoint.x - x));
    }
}
