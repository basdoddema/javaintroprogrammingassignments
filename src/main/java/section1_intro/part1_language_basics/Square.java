package section1_intro.part1_language_basics;

public class Square {
    Point upperLeft;
    Point lowerRight;

    /**
     * returns the surface defined by the rectangle with the given upper left and lower right corners.
     * It assumes two corners have been created already!
     * @return
     */
    int surface(){
        int length = upperLeft.y - lowerRight.y;
        int width = lowerRight.x - upperLeft.x;
        int output = length * width;
        return output;
    }
}
