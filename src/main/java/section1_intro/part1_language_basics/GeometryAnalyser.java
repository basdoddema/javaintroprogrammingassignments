package section1_intro.part1_language_basics;

public class GeometryAnalyser {
    public static void main(String[] args) {
        int x1 = Integer.parseInt(args[0]);
        int y1 = Integer.parseInt(args[1]);
        int x2 = Integer.parseInt(args[2]);
        int y2 = Integer.parseInt(args[3]);
        String method = args[4];
        Point upperLeft = new Point();
        upperLeft.x = x1;
        upperLeft.y = y1;
        Point lowerRight = new Point();
        lowerRight.x = x2;
        lowerRight.y = y2;
        switch(method) {
            case "surf":
                Square square = new Square();
                square.upperLeft = upperLeft;
                square.lowerRight = lowerRight;
                int outputS = square.surface();
                System.out.println(outputS);
                break;
            case "dist":
                double outputD = Math.round(upperLeft.euclideanDistanceTo(lowerRight) * 10) / 10.0;
                System.out.println(outputD);
                break;
        }




        //your code here
    }
}
